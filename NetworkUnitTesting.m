function [ mseTrain, mseTest ] = NetworkUnitTesting( X,Y,netPrototype,iterations,cvMode,limit )
    
    mseTrain = zeros(iterations,1);
    mseTest = zeros(iterations,1);
    
    parfor id = 1:iterations
        % Create random permutation of dataset
        datasetPerm = randperm(size(X,2)-1);
        
        testX = X(:,datasetPerm);
        testY = Y(:,datasetPerm);
        
        if limit < size(X,2) 
            testX = testX(:,1:limit);
            testY = testY(:,1:limit);
        end
        % Run cross-validation test
        [mseTrain(id,1), mseTest(id,1)] = CrossValidationTest( testX,testY,netPrototype,cvMode );
    end
end

