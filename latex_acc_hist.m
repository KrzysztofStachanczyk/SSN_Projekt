clear all;

% Configuration
resultsSubdirectory='./trainingFunctionsTestsResults/';

resultFile = 'Iris.mat';
resultOutputPrefix = 'Iris';
bins = [(0.87):(0.2*10^-2):(1)];
% - Training functions for histogram 
functions = {'trainrp','trainscg'};

% Add 3Party functions directory to source path
addpath('./thirdPartyTools/');

% Load result file
load([resultsSubdirectory,resultFile]);

% Training MSE
figure;
clf;
for i = 1:size(functions,2)
    fun = functions{1,i}
    
    MSE = [results.(fun)(:).accuracyTrain];
    
    if i == 1
        hold on;
    end
    
    %histogram(MSE,bins,'FaceAlpha',0.5);    
    histogram(MSE,bins,'FaceAlpha',0.5);    
end
grid on;
xlabel('Skutecznosc');
title('Histogram skutecznosci dla zbioru uczacego');
legend(functions);
hold off;
print(['./plots/',resultOutputPrefix,'_ACC_train_hist.png'],'-dpng');

% Test MSE
figure;
clf;
for i = 1:size(functions,2)
    fun = functions{1,i}
    
    MSE = [results.(fun)(:).accuracyTest];
    
    if i == 1
        hold on;
    end
    
    histogram(MSE,bins,'FaceAlpha',0.5);    
end
grid on;
xlabel('Skutecznosc');
title('Histogram skutecznosci dla zbioru testowego');
legend(functions);
hold off;

print(['./plots/',resultOutputPrefix,'_ACC_test_hist.png'],'-dpng');
