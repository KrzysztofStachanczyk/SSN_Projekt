% Example of working data loader
loaderFunction = @readWpbc;

addpath('dataLoaders');
[X,Y] = loaderFunction();

assert(isnumeric(X),'X is not numeric vector');
assert(isnumeric(Y),'Y is not numeric vector');

assert(all(all(isfinite(X))),'X contains non finite values');
assert(all(all(isfinite(Y))),'Y contains non finite values');

fprintf('Zaladowano %d wekorow %d cech\n',size(X,1),size(X,2));
fprintf('Dane wyjsciowe skladaja sie z %d wekorow %d wartosci\n',size(Y,1),size(Y,2));