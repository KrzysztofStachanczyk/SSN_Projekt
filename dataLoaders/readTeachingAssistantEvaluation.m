function [ X,Y ] = readTeachingAssistantEvaluation( )

    M = csvread('datasets/tae.data');
    %Y = M(:,6);
    X = zeros(size(M,1),54);
    X(:,1) = M(:,1);
    for i = 1 : size(M,1)
        %Course instructor (categorical, 25 categories)
        course_instructor = M(i,2);
        X(i,1 + course_instructor) = 1;
        %Course (categorical, 26 categories)
        course = M(i,3);
        X(i,26 + course) = 1;
    end
    X(:,53) = M(:,4);
    X(:,54) = M(:,5);
    
    Y = zeros(size(M,1),3);
   
   for i = 1:size(M,1)
      for j= 1:3
          if(M(i,6)==j)
              Y(i,j)=1;
          end
      end
   end

end

