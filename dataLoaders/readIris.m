function [ X, Y ] = readIris( )
%Read wine dataset
   	load iris_dataset;
    Y = irisTargets';
    X = irisInputs';


end

