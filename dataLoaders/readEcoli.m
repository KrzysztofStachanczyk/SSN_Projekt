function [ X,Y ] = readEcoli()
%Read ecoli Data Set set
   M = csvread('datasets/ecoli.data');
   X = M(:,1:7);
   
   Y = zeros(size(M,1),8);
   
   for i = 1:size(M,1)
      for j= 1:8
          if(M(i,8)==j)
              Y(i,j)=1;
          end
      end
   end
end

