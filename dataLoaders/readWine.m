function [ X, Y ] = readWine( )
%read wine dataset
    load wine_dataset;
    Y = wineTargets';
    X = wineInputs';

end

