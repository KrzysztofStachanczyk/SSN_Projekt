function [ X, Y ] = readMachineLearning( )
%Read statlogAustralian dataset (description in statlogAustralian.names.txt)
   	M = csvread('datasets/machineLearning.data');
    Y = M(:,257:end);
    X = M(:,1:256);
end

