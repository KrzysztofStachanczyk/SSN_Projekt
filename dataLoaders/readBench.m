function [ X,Y ] = readBench()
%Read Bench Data Set  dataset
    M = csvread('datasets/bench.data',0,0,[0 0 60 60]);
    Y = M(:,60);
    X = M(:,60);
end

