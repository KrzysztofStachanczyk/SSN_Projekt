function [ X,Y ] = readGlass()
%Read glass Data Set set
   M = csvread('datasets/glass.data');
   X = M(:,2:10);
   
   Y = zeros(size(M,1),7);
   
   for i = 1:size(M,1)
      for j= 1:7
          if(M(i,11)==j)
              Y(i,j)=1;
          end
      end
   end
end

