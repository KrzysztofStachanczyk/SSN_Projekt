function [ X,Y ] = readWifiLocalization()
%Read Wireless Indoor Localization Data Set (output is room ID - for each room single output)
% Inputs are signal samples
   M = csvread('datasets/wifi_localization.data');
   X = M(:,1:7);
   Y = zeros(size(M,1),4);
   
   for i = 1:size(M,1)
      for j= 1:4
          if(M(i,8)==j)
              Y(i,j)=1;
          end
      end
   end

end

