function [ X,Y ] = readHeart()
%Read Heart Data Set  dataset
    M = csvread('datasets/heart.data');
    Y = M(:,13);
    X = M(:,1:13);
end

