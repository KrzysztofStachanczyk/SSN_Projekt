function [ X, Y ] = readLenses()
     M = csvread('datasets/lenses.data');
     X = M(:,2:5);
     %Y = M(:,6);
     
    Y = zeros(size(M,1),3);
    for i = 1:size(M,1)
      for j= 1:3
          if(M(i,6)==j)
              Y(i,j)=1;
          end
      end
   end
end

