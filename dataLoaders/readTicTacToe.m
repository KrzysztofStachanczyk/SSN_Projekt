function [ X,Y ] = readTicTacToe( )

     M = csvread('datasets/tic-tac-toe.data');
     X = M(:,1:9);
     Y = M(:,10);

end

