function [ X, Y ] = readStatlogAustralian( )
%Read statlogAustralian dataset (description in statlogAustralian.names.txt)
   	M = csvread('datasets/machineLearning.data');
    Y = M(:,15);
    X = M(:,1:14);
end

