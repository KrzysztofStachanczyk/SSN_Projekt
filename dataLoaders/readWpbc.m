function [ X,Y ] = readWpbc()
%Read Breast Cancer Wisconsin (Diagnostic) Data Set  dataset (description in wpbc.names.txt)
    M = csvread('datasets/wpbc.data');
    Y = M(:,2);
    % 3 - nie dotyczy
    X = M(:,4:end);
end

