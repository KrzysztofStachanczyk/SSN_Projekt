function [ X,Y ] = readVertebral3C( )

    M = csvread('datasets/Vertebral_column_3C.data');
    X = M(:,1:6);
    %Y = M(:,7);

    Y = zeros(size(M,1),3);
    for i = 1:size(M,1)
      for j= 1:3
          if(M(i,7)==j)
              Y(i,j)=1;
          end
      end
   end
end

