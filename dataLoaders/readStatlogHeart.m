function [ X,Y ] = readStatlogHeart( )
%Read statlogHeart dataset (description in statlogHeart.names.txt)
   	M = csvread('datasets/statlogHeart.data');
    [sizeRow, sizeCol] = size(M);
    for i = 1:sizeRow
       if M(i,14) == 2
           M(i,14) = 0;
       end
    end
    Y = M(:,14);
    X = M(:,1:13);
end

