function [ X,Y ] = readMagic()
%Read Steel Plates Faults Data Set set
% (description magic04.names.txt)
   M = csvread('datasets/magic04.data');
   Y = M(:,11);
   X = M(:,1:10);
end

