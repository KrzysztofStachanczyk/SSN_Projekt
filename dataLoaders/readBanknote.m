function [ X,Y ] = readBanknote()
%Read Bridges Data Set  dataset
    M = csvread('datasets/banknote.data');
    Y = M(:,5);
    X = M(:,1:4);
end

