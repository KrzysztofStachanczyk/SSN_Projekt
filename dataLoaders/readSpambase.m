function [ X,Y ] = readSpambase()
%Read Bridges Data Set  dataset
    M = csvread('datasets/spambase.data');
    Y = M(:,58);
    X = M(:,1:54);
end

