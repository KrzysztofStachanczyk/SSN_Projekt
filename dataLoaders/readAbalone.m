function [ X,Y ] = readAbalone()
%Read Bridges Data Set  dataset
   M = csvread('datasets/abalone.data');
   X = M(:,1:8);
   
   Y = zeros(size(M,1),29);
   
   for i = 1:size(M,1)
      for j= 1:29
          if(M(i,9)==j)
              Y(i,j)=1;
          end
      end
   end
end

