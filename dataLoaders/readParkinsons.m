function [ X,Y ] = readParkinsons()
%Read parkinsons dataset (description in parkinsons.names.txt)
    M = csvread('datasets/parkinsons.data',1,1);
    Y = M(:,17);
    X = [M(:,2:16) M(:,18:end)];
end

