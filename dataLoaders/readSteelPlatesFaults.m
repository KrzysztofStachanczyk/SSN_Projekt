function [ X,Y ] = readSteelPlatesFaults(  )
%Read Steel Plates Faults Data Set set
% (description Faults27x7_var.names.txt)
   M = csvread('datasets/Faults.data');
   Y = M(:,28:end);
   X = M(:,1:27);
end

