function [ X, Y ] = readPokerHand( )
%Read pokerHand dataset (description in pokerHand.names.txt)
   	M = csvread('datasets/pokerHand.data');
    X = M(:,1:10);
    Y = zeros(size(M,1),10);
   
    for i = 1:size(M,1)
        for j= 0:9
            if(M(i,11)==j)
                Y(i,j+1)=1;
            end
        end
    end
end

