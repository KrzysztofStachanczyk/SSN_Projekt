function [ X,Y ] = readVertebral2C( )

    M = csvread('datasets/Vertebral_column_2C.data');
    X = M(:,1:6);
    Y = M(:,7);

end

