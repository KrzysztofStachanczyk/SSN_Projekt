function [ avgMSETrain, avgMSETest ] = CrossValidationTest( X,Y,netPrototype,cvMode )
    
    mseTrain = zeros(cvMode,1);
    mseTest = zeros(cvMode,1);

    for id=1:cvMode
        newNet = netPrototype;
        idx = 1:size(X,2);
        
        % Split datasets 
        testIdx = idx(mod(idx,cvMode)==(id-1));
        learningIdx = idx(mod(idx,cvMode)~=(id-1));
        
        
        learningInputs = X(:,learningIdx);
        expectedLearningOutputs = Y(:,learningIdx);
        
        testInputs = X(:,testIdx);
        expectedTestOutputs = Y(:,testIdx);
        
        % Train network
        newNet = train(newNet,learningInputs,expectedLearningOutputs);
        
        % Run network on datasets
        learningOutputs = newNet(learningInputs);
        testOutputs = newNet(testInputs);
        
        % Calculate MSE error
        mseTrain(id,1) = immse(learningOutputs,expectedLearningOutputs);
        mseTest(id,1) = immse(testOutputs,expectedTestOutputs);

    end

    % Calculate metrics 
    avgMSETrain = mean(mseTrain);
    avgMSETest = mean(mseTest);
end

