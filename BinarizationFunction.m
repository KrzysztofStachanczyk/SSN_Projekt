function [ result ] = BinarizationFunction( detectionsVector )
    % Condition 1: Simple imlementation output class is detected if 
    
    % stimulation > 0.5
    %result = detectionsVector>0.5;
    
    % Condition 2: Only one active there is not zero class
    
    A = max(detectionsVector,[],1);
    result = zeros(size(detectionsVector));
    
    for i = 1:size(detectionsVector,2)
        for j = 1:size(detectionsVector,1)
            if detectionsVector(j,i)==A(1,i)
                result(j,i) = 1;
            end
        end
    end
    
end

