clear all;

% Configuration
resultsSubdirectory='./trainingFunctionsTestsResults/';

resultFile = 'PokerHand.mat';
resultOutputPrefix = 'PokerHand';
% - Specify confusion classes
classes = {'Nothing in hand', 'One pair','Two pairs','Three of a kind','Straight','Flush','Full house','Four of a kind','Straight flush','Royal flush','Other faults '};

% Add 3Party functions directory to source path
addpath('./thirdPartyTools/');

% Load result file
load([resultsSubdirectory,resultFile]);

% Fetch training functions names (which are structure fields)
functions = fieldnames(results);

for i = 1:size(functions,1)
    fun = functions{i,1}
    table{i+1,1} = fun;
    
    confusionTable = cell(size(classes,2)+1,size(classes,2)+1);
    
    % Setup vertical and horizontal headers
    confusionTable(2:end,1) = classes';
    confusionTable(1,2:end) = classes;
    
    trainingFunctionResult = results.(fun);
    
    confMatrixSize = size(trainingFunctionResult(1).cMatrixTest,1);
    dataRecorsNumber = size(trainingFunctionResult,2);
    
    % Allocate space
    confusionMatrixTest = zeros(confMatrixSize,confMatrixSize);
    confusionMatrixTrain = zeros(confMatrixSize,confMatrixSize);
    
    for j = 1:dataRecorsNumber
       confusionMatrixTest = confusionMatrixTest + trainingFunctionResult(j).cMatrixTest(:,:); 
       confusionMatrixTrain = confusionMatrixTrain + trainingFunctionResult(j).cMatrixTrain(:,:); 
    end
    
    % Convert to percent
    confusionMatrixTest = 100*confusionMatrixTest./sum(sum(confusionMatrixTest));
    confusionMatrixTrain = 100*confusionMatrixTrain./sum(sum(confusionMatrixTrain));
    
    
    % Save cell tables as Latex tabular    
    confusionTable(2:end,2:end) = num2cell(confusionMatrixTest);
    latextable(confusionTable,'name',['./latexReports/',resultOutputPrefix,'_',fun, ...
        '_confusion_matrix_test.tex'])
    
    
    confusionTable(2:end,2:end) = num2cell(confusionMatrixTrain);
    latextable(confusionTable,'name',['./latexReports/',resultOutputPrefix,'_',fun, ...
        '_confusion_matrix_train.tex'])
end