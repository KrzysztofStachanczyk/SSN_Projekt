clear all;

% Configuration
resultsSubdirectory='./trainingFunctionsTestsResults/';

resultFile = 'PokerHand.mat';
resultOutputPrefix = 'PokerHand';

plotTitle = 'Wykres zmian MSE zbioru uczacego w funkcji ilosci iteracji';

% Add 3Party functions directory to source path
addpath('./thirdPartyTools/');

% Load result file
load([resultsSubdirectory,resultFile]);

% Fetch training functions names (which are structure fields)
functions = fieldnames(results);

clf;
for i = 1:size(functions,1)
    fun = functions{i,1}
    
    perfLen = 250;
    dataRecorsNumber = size(results.(fun),2);
    
    epochs = ceil(max([results.(fun)(:).avgEpochNumber]));
    
    % Calculate mean value of performance
    meanOfPerf = zeros(1,perfLen);
    for j = 1:dataRecorsNumber
         t = results.(fun)(j).perf;
         meanOfPerf = meanOfPerf + t(1,1:perfLen);
    end
    meanOfPerf= meanOfPerf./perfLen;
    
    % Plot data
    if i<6
        semilogy(meanOfPerf(1,1:epochs)');
    else
        semilogy(meanOfPerf(1,1:epochs)','.');
    end
    if i==1
        hold on;
    end
end
grid on;

% Setup legend and title
legend(functions);
xlabel('Numer epoki');
ylabel('MSE zbioru uczacego');
title(plotTitle);

hold off;

%Save plot
print(['./plots/',resultOutputPrefix,'_MSE_evolution_plot.png'],'-dpng')

