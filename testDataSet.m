clear all;

% Resolve issue with lack of the MPI
distcomp.feature( 'LocalUseMpiexec', false );

% Add dataLoaders directory to source search path
addpath('dataLoaders');

%Load data from set

[X,Y] = readPokerHand();


%Configuration:
% - cross-validation type
cvMode = 10;

% - number of test iterations
unitTests = 50;

dataSetLimit = 20000000;
dataSetName = 'PokerHand';

% - topology setup
hiddenLayersSize = [2, 2];
hiddenLayersFunction = {'purelin', 'purelin'};

functions = GetListOfTestedTrainFunctions();
results = struct();

trainTime = []
for i = 1:size(functions,1)
    tic
    functions{i,1}
    
    % Configure neural network topology
    net = feedforwardnet(hiddenLayersSize);
    net = configure(net, X', Y');
    
    for j = 1:length(hiddenLayersFunction)
        net.layers{j}.transferFcn=hiddenLayersFunction{j};
    end
    
    % Define neural network training parameters
    net.trainFcn = functions{i,1};
    net.divideParam.testRatio = 0;
    net.trainParam.epochs=250;
    net.trainParam.showWindow = false;
    net.trainParam.showCommandLine = false;
    
    view(net);
    pause(10);
    % Perform unit testing
    [sumup] = NetworkUnitTestingExt( X',Y',net,unitTests,cvMode,dataSetLimit );
    struct2table(sumup)
    
    % Store results 
    results.(functions{i,1})=sumup;
    toc
    trainTime = [trainTime; toc]
end
save(['./trainingFunctionsTestsResults/',dataSetName],'results')

