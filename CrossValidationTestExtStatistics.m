function [cvResults ] = CrossValidationTestExtStatistics( X,Y,netPrototype,cvMode,outputClasses )    
    
    % Allocate memory
    mseTrain = zeros(cvMode,1);
    mseTest = zeros(cvMode,1);
    
    epochNumber = zeros(cvMode,1);
    
    accuracyTrain = zeros(cvMode,1);
    accuracyTest = zeros(cvMode,1);
        
    perf = zeros(cvMode,netPrototype.trainParam.epochs);
    
    cMatrixTrain = [];
    cMatrixTest = [];

    for id=1:cvMode
        % Copy network prototype
        newNet = netPrototype;
                
        % Split datasets 
        idx = 1:size(X,2);
        testIdx = idx(mod(idx,cvMode)==(id-1));
        learningIdx = idx(mod(idx,cvMode)~=(id-1));
        
        learningInputs = X(:,learningIdx);
        expectedLearningOutputs = Y(:,learningIdx);
        
        testInputs = X(:,testIdx);
        expectedTestOutputs = Y(:,testIdx);
        
        % Train network
        [newNet, tr] = train(newNet,learningInputs,expectedLearningOutputs);
        
        % Store training metrics
        epochNumber(id,1) = tr.num_epochs;
        perf(id,1:size(tr.perf,2))=tr.perf; 
        perf(id,size(tr.perf,2):end)=tr.perf(1,size(tr.perf,2));
        
        % Run network on datasets
        learningOutputs = newNet(learningInputs);
        testOutputs = newNet(testInputs);
        
        % Calculate MSE error
        mseTrain(id,1) = immse(learningOutputs,expectedLearningOutputs);
        mseTest(id,1) = immse(testOutputs,expectedTestOutputs);
        
        % Generate training set confusion matrix
        [c_matrix,~]= confusionMatrix(expectedLearningOutputs,BinarizationFunction(learningOutputs),outputClasses);
        if id==1
            cMatrixTrain = c_matrix;
        else
            cMatrixTrain = cMatrixTrain+c_matrix;
        end
        
        % Generate test set confusion matrix
        [c_matrix,~]= confusionMatrix(expectedTestOutputs,BinarizationFunction(testOutputs),outputClasses);
        if id==1
            cMatrixTest = c_matrix;
        else
            cMatrixTest = cMatrixTest+c_matrix;
        end

        % Calculate accuracy for training set
        for j=1:size(learningOutputs,2)
           if isequal(BinarizationFunction(learningOutputs(:,j)),expectedLearningOutputs(:,j))
              accuracyTrain(id,1)=accuracyTrain(id,1)+1; 
           end
        end
        accuracyTrain(id,1) = accuracyTrain(id,1)/size(learningOutputs,2);
        
        % Calculate accuracy for testing set
        for j=1:size(testOutputs,2)
           if isequal(BinarizationFunction(testOutputs(:,j)),expectedTestOutputs(:,j))
              accuracyTest(id,1)=accuracyTest(id,1)+1; 
           end
        end
        accuracyTest(id,1) = accuracyTest(id,1)/size(testOutputs,2);
        
    end

    % Calculate metrics 
    cvResults=struct();
    cvResults.avgMSETrain = mean(mseTrain);    
    cvResults.avgMSETest = mean(mseTest);
                
    cvResults.accuracyTrain = mean(accuracyTrain);
    cvResults.accuracyTest = mean(accuracyTest);

    cvResults.cMatrixTrain = cMatrixTrain./cvMode; 
    cvResults.cMatrixTest = cMatrixTest./cvMode;
        
    cvResults.perf = mean(perf);
    cvResults.performFcn = netPrototype.performFcn;

    cvResults.avgEpochNumber = mean(epochNumber);
    
end

