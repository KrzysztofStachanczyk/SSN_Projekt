function [ allVariants, minMSETrain,avgMSETrain,maxMSETrain,minMSETest,avgMSETest,maxMSETest ] = FindOptimalTopology( X,Y,hiddenLayers,maxLayerSize,hiddenTransferFcn,limit )
    
    % Create all variants of topology
     allVariants = permn([1:maxLayerSize],hiddenLayers);
    %allVariants = permn([1:3:maxLayerSize],hiddenLayers);
    variantsNumber = size(allVariants,1);
    
    avgMSETrain = zeros(size(variantsNumber,1),1);
    avgMSETest = zeros(size(variantsNumber,1),1);
    
    minMSETrain = zeros(size(variantsNumber,1),1);
    minMSETest = zeros(size(variantsNumber,1),1);
    
    maxMSETrain = zeros(size(variantsNumber,1),1);
    maxMSETest = zeros(size(variantsNumber,1),1);
    
    for i = 1:variantsNumber
        fprintf('%d/%d\n',i,variantsNumber);
        
        % Create and configure network
        net = feedforwardnet(allVariants(i,:));
        net = configure(net, X, Y);

        net.divideParam.testRatio = 0;
        net.trainParam.epochs=250;

        net.trainParam.showWindow = false;
        net.trainParam.showCommandLine = false;
        
        
        for j= 1:hiddenLayers
            disp(j)
            disp(hiddenTransferFcn(1,j))
            fName = hiddenTransferFcn{1,j};
            net.layers{j}.transferFcn = char(fName);
        end
        
        % Show network topology (only first variant) - for debug
        if i==1
            view(net);
            pause(0.5);
        end
        
        % Run unit test on network (for example 50 samples and cv5)
        [ mseTrain, mseTest ] = NetworkUnitTesting( X,Y,net,10,5,limit );
        
        % Calculate metrics
        avgMSETrain(i,1) = mean(mseTrain);
        avgMSETest(i,1) = mean(mseTest);
        
        minMSETrain(i,1) = min(mseTrain);
        minMSETest(i,1) = min(mseTest);
        
        maxMSETrain(i,1) = max(mseTrain);
        maxMSETest(i,1) = max(mseTest);
        
    end
end

