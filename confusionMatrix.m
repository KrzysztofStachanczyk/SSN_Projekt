function [cMatrix,classRef]= confusionMatrix(expected,result,outputClasses)
    expected = bi2de(expected');
    result = bi2de(result');
    
    % Check if vectors are equal 
    if length(expected) ~= length(result)
        error('Input have different lengths')
    end
                               
    classNumber=length(outputClasses);
    
    cMatrix=zeros(classNumber+1);
    classRef=cell(classNumber,1);
    
    % Calculate confusion matrix
    for i=1:classNumber
        classRef{i,1}=strcat('class',num2str(i),'==>',num2str(outputClasses(i)));
        
        for j=1:classNumber
            val=(expected==outputClasses(i)) & (result==outputClasses(j));
            cMatrix(i,j)=sum(val);
        end
    end
            
    % Attach informations about detections of not existing classes
    cMatrix(classNumber+1,classNumber+1) = length(result) - sum(sum(cMatrix));
end
