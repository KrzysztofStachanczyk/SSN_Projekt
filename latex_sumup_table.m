clear all;

% Configuration
resultsSubdirectory='./trainingFunctionsTestsResults/';

resultFile = 'PokerHand.mat';
resultOutputPrefix = 'PokerHand';


% Add 3Party functions directory to source path
addpath('./thirdPartyTools/');

% Load result file
load([resultsSubdirectory,resultFile]);

% Fetch training functions names (which are structure fields)
functions = fieldnames(results);

% Specify table header
table = {'Nazwa funkcji','Srednia ilosc epok','Skutecznosc na zbiorze ucz?cym',...
    'Skutecznosc na zbiorze testowym','MSE zbi�r uczacy','MSE zbior testowy'};

for i = 1:size(functions,1)
    fun = functions{i,1}
    table{i+1,1} = fun;
    
    % Calculate average value of metrics
    table{i+1,2} = mean([results.(fun)(:).avgEpochNumber],2);    
    table{i+1,3} = mean([results.(fun)(:).accuracyTrain],2);
    table{i+1,4} = mean([results.(fun)(:).accuracyTest],2);
    table{i+1,5} = mean([results.(fun)(:).avgMSETrain],2);
    table{i+1,6} = mean([results.(fun)(:).avgMSETest],2);
end

% Save cell table as Latex tabular
latextable(table,'name',['./latexReports/',resultOutputPrefix,'_sumup_table.tex'])

