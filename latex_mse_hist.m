clear all;

% Configuration
resultsSubdirectory='./trainingFunctionsTestsResults/';

resultFile = 'PokerHand.mat';
resultOutputPrefix = 'PokerHand';
bins = [(0.05):(0.05*10^-2):(0.06)];
% - Training functions for histogram 
functions = {'traincgp', 'traincgf'};


% Add 3Party functions directory to source path
addpath('./thirdPartyTools/');

% Load result file
load([resultsSubdirectory,resultFile]);

% Training MSE
figure;
clf;
for i = 1:size(functions,2)
    fun = functions{1,i}
    
    MSE = [results.(fun)(:).avgMSETrain];
    
    if i == 1
        hold on;
    end
    
    %histogram(MSE,bins,'FaceAlpha',0.5);    
    histogram(MSE,bins,'FaceAlpha',0.5);    
end
grid on;
xlabel('MSE -sredni kwadrat bledu');
title('Histogram bledu dla zbioru uczacego');
legend(functions);
hold off;
print(['./plots/',resultOutputPrefix,'_MSE_train_hist.png'],'-dpng');

% Test MSE
figure;
clf;
for i = 1:size(functions,2)
    fun = functions{1,i}
    
    MSE = [results.(fun)(:).avgMSETest];
    
    if i == 1
        hold on;
    end
    
    histogram(MSE,bins,'FaceAlpha',0.5);    
end
grid on;
xlabel('MSE -sredni kwadrat bledu');
title('Histogram bledu dla zbioru testowego');
legend(functions);
hold off;

print(['./plots/',resultOutputPrefix,'_MSE_test_hist.png'],'-dpng');