clear all;

% Resolve lack of MPI issue 
distcomp.feature( 'LocalUseMpiexec', false );

% Add loaders functions directory to source path.
addpath('dataLoaders');

% Load data
[X,Y] = readHaberman();

% Define variants of hidden layers topology
setups = {
    {'tansig'};
    {'tansig','tansig'};
    {'purelin'};
    {'purelin','purelin'};
    {'tansig','purelin'};
    {'purelin','tansig'}
};
maxNeronsPerHiddenLayer = 6;
maxHiddenLayers = 2;
limit = 50;

result =[];

for i=1:size(setups,1);
    tic;
        setup = setups{i,1};
        [ allVariants, minMSETrain,avgMSETrain,maxMSETrain,minMSETest,avgMSETest,maxMSETest ] = FindOptimalTopology( X',Y',size(setup,2),maxNeronsPerHiddenLayer,setup,limit );
        
        allVariantsWithOffset = zeros(size(allVariants,1),maxHiddenLayers);
        allVariantsWithOffset(:,1:size(allVariants,2))=allVariants(:,:);
        
        idx = ones(size(allVariants,1),1)*i;
        if i==1           
            result = [idx allVariantsWithOffset minMSETest avgMSETest maxMSETest];
        else
            result = [result;idx allVariantsWithOffset minMSETest avgMSETest maxMSETest];
        end
    toc
end

