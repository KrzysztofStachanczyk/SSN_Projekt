function [ sumUp ] = NetworkUnitTestingExt( X,Y,netPrototype,iterations,cvMode,limit )

    % Prepare structure
    sumUp = struct('avgMSETrain',[],'avgMSETest',[],...
        'accuracyTrain',[],'accuracyTest',[],...
        'cMatrixTrain',[],'cMatrixTest',[],...
        'perf',[],'performFcn',[],'avgEpochNumber',[]);
    
    % Find unique classes and encode them as integers
    outputClasses = unique(bi2de(Y'));
    
    % Perform unit testing
    parfor id = 1:iterations
        
        % Create random permutation of dataset
        datasetPerm = randperm(size(X,2)-1);
        
        testX = X(:,datasetPerm);
        testY = Y(:,datasetPerm);

        % Limit data vector length
        if limit < size(X,2) 
            testX = testX(:,1:limit);
            testY = testY(:,1:limit);
        end
        
        % Run cross-validation test
        [cvResults] = CrossValidationTestExtStatistics( testX,testY,netPrototype,cvMode,outputClasses );
        
        % Store results
        sumUp(id) = cvResults;
    end
end

